from pybitbucket.bitbucket import Client, RepositoryType
from pybitbucket.auth import BasicAuthenticator, OAuth1Authenticator
from pybitbucket.team import TeamRole, Team
from pybitbucket.repository import Repository, RepositoryPayload, RepositoryForkPolicy, PayloadBuilder
from pybitbucket.user import User


class BitbucketApi:

    def get_client(self, api_key, api_secret):
        return Client(OAuth1Authenticator(api_key,api_secret))
    
    def get_user(self, client):
        return User.find_current_user(client)

    def get_teams(self, client):
        user_teams = []
        for i,team in enumerate(Team.find_teams_for_role(role="member", client=client)):
            user_teams.append(team)
        return user_teams
   
    def create_repository(self, client, repo_name, owner, privacy = True):
        
        if privacy:
            payload = RepositoryPayload({
                "fork_policy": RepositoryForkPolicy.NO_PUBLIC_FORKS,
                "is_private": True
            }, owner = owner.data["username"])
        else:
            payload = RepositoryPayload({
                "fork_policy": RepositoryForkPolicy.ALLOW_FORKS,
                "is_private": False
            }, owner = owner.data["username"])
            
        return Repository.create(
            payload=payload,
            repository_name = repo_name,
            owner=owner.data["username"],
            client=client
        )