
.. image:: https://img.shields.io/pypi/v/datakit-bitbucket.svg
        :target: https://pypi.python.org/pypi/datakit-bitbucket

.. image:: https://img.shields.io/pypi/pyversions/datakit-bitbucket.svg
        :target: https://pypi.python.org/pypi/datakit-bitbucket

.. image:: https://img.shields.io/travis/quickhand/datakit-bitbucket.svg
        :target: https://travis-ci.org/quickhand/datakit-bitbucket

.. image:: https://readthedocs.org/projects/datakit-bitbucket/badge/?version=latest
        :target: https://datakit-bitbucket.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status


===============================
DataKit Bitbucket plugin
===============================

Commands to manage project integration with Bitbucket.

Overview
========

Light-weight Bitbucket integration for DataKit workflows.

* Documentation: http://datakit-bitbucket.readthedocs.io/en/latest/
* Bitbucket: https://bitbucket.org/quickhand/datakit-bitbucket
* PyPI: https://pypi.python.org/pypi/datakit-bitbucket
* Free and open source software: `ISC license`_

.. _ISC license: https://bitbucket.org/quickhand/datakit-bitbucket/src/master/LICENSE

Features
========

This plugin is designed to streamline the process of generating a Bitbucket project
and linking it to a local directory containing a newly created project skeleton.
It's intended to be used in tandem with the datakit-project_ plugin, which
helps generate project skeletons for data work and software development.

The plugin provides the following features:

* Interactive selection of Bitbucket account for project creation
* Automated creation of a Bitbucket project
* Ability to set privacy level of repo
* Automated execution of git commands to bootstrap a project (init/add/commit)
* Linking of local Git repository to newly created Bitbucket project
* Automated "push" of initial commit to new Bitbucket project

Install/set-up
==============

First, some preliminary Bitbucket setup that will allow you to push and pull code securely:

* Create a `Bitbucket`_ account.
* `Generate an ssh keypair`_ (if you haven't already)
* `Add your ssh public key to Bitbucket`_

Next, install the core datakit_ library and the datakit-project_ and datakit-bitbucket_ plugins:

.. code::

   pip install --user datakit-core datakit-project datakit-bitbucket


Next, you must generate a `Bitbucket OAuth Consumer`_ with the permissions to read account, team and project information, 
as well as to read, write and admin repositories. This will enable datakit to automate interactions with the 
Bitbucket API (e.g. to auto-generate a project on Bitbucket).

With the API key and secret in hand, store it in the configuration file for the *datakit-bitbucket* plugin.
This file must be located inside the home directory for datakit configurations.

On Mac/Linux systems, you should create `~/.datakit/plugins/datakit-bitbucket/config.json` file with
the below commands::

  # First ensure the plugin directory exists
  mkdir -p ~/.datakit/plugins/datakit-bitbucket/
  # Create a blank configuration file
  touch ~/.datakit/plugins/datakit-bitbucket/config.json

Lastly, edit the newly created file so that it contains the below, replacing
`BITBUCKET_API_KEY` and `BITBUCKET_API_SECRET` with your actual key and secret::

.. code-block:: JSON
  
   {
      "bitbucket_api_key": "BITBUCKET_API_KEY",
      "bitbucket_api_secret": "BITBUCKET_API_SECRET"
   }

.. note::

   Please be sure to avoid extraneous white space and preserve the double quotes. This must be valid JSON!!


Usage
=====

Create a new project using the datakit-project_ plugin.  For example,
you could try AP's template for R-based projects::

    datakit project create -t https://github.com/associatedpress/cookiecutter-r-project.git

The above command will prompt you for various bits of information, including a project name.
Let's assume we chose `awesome-proj` as the name.

To integrate the project with Bitbucket, navigate into the newly created
project directory and execute the following command::

   cd awesome-proj
   datakit bitbucket integrate

The above should trigger a series of prompts that allow you to select the Bitbucket account where the
new repo should be created, and whether to make the repo private or public.

The command will auto-generate the Bitbucket project, link your local code to the new project, and
push the newly generated project skeleton to Bitbucket as your first commit.

.. note:: The command will warn you if a project name already exists on the Bitbucket account.
   In such a case, you must either choose a different account (e.g. if
   you're a member of another Bitbucket organization), rename the project, or remove the previously
   existing project from Bitbucket.


.. _datakit: https://github.com/associatedpress/datakit-core
.. _datakit-bitbucket docs: https://datakit-bitbucket.readthedocs.io/en/latest/
.. _datakit-project: https://datakit-project.readthedocs.io/en/latest/