# -*- coding: utf-8 -*-
from cliff.command import Command
from .bitbucket_api import BitbucketApi
from .project_mixin import ProjectMixin
from .repository import Repository

def ask(question):
    return input(question)



class Integrate(ProjectMixin, Command, BitbucketApi):
    "Integrate local project code with Bitbucket"

    def take_action(self, parsed_args):
        
        api_key = self.configs.get('bitbucket_api_key')
        api_secret = self.configs.get('bitbucket_api_secret')
        if not api_key or not api_secret:
            err_msg = "You must configure a Bitbucket API key and secret to use this command!!\n"
            self.log.error(err_msg)
        elif Repository.initialized():
            self.log.error("\nERROR: Repo has already been initialized locally!!")
            self.log.error(
                "You must either remove the .git/ directory " +
                "before re-running this command, or manually " +
                "configure Github integration.\n"
            )
        else:
            bitbucket = self.get_client(api_key,api_secret)
            user_account = self.get_user(bitbucket)
            user_teams = self.get_teams(bitbucket)
            target_account = user_account
            if len(user_teams) != 0:
                msg = "Choose an account where the new project should be created:\n"
                self.log.info(msg)
                self.log.info("(1) User: {}".format(user_account.data["display_name"]))
                for i, team in enumerate(user_teams):
                    self.log.info("({}) Team: {}".format(i+2, team.data["display_name"]))

                choice_msg = "\nType a number or leave blank for default [{}]: ".format(user_account.data["display_name"])


                choice = None
                while not choice:
                    str_choice = ask(choice_msg)
                    if str_choice.strip() == '':
                        choice = 1
                    else:
                        try:
                            choice = int(str_choice)
                        except:
                            pass    
                        if choice<1 or choice>len(user_teams)+1:
                            choice = None

                target_account = user_account if choice == 1 else user_teams[choice-2]
                self.log.info("You chose: {}".format(target_account.data["display_name"]))
                confirm = ask("Is this correct? y/n [y]: ").strip().lower()
                if confirm in ["y", ""]:
                    privacy_choice = ask("Should this repo be private? y/n [y]: ").strip().lower()
                    privacy = True if privacy_choice in ['', 'y'] else False
                    try:
                        resp = self.create_repository(bitbucket, self.project_slug, target_account, privacy)

                        html_url = [x["href"] for x in resp.data["links"]["clone"] if x["name"] == "https"][0]
                        ssh_url = [x["href"] for x in resp.data["links"]["clone"] if x["name"] == "ssh"][0]

                        self.log.info("Repo created at {}".format(html_url))
                        self.log.info("Running local Git initialization...")
                        # TODO: Create a project-level config/datakit-bitbucket.json?
                        # containing name of selected account and possibly account type (org or user)?
                        # This can be used downstream to configure org or user-specific API calls
                        # if any (hold off on the "type" config until we
                        # determine if there are different call ypes)
                        Repository.init()
                        Repository.add()
                        Repository.commit("Initial commit")
                        Repository.add_remote(ssh_url)
                        alert_msg = 'Local repo linked to remote origin: \n\t{}'.format(html_url)
                        self.log.info(alert_msg)
                        Repository.push()
                        self.log.info("First commit made locally and pushed to remote")
                        self.log.info("View the project on Bitbucket at {}".format(html_url))
                    except:
                        msg = "\nERROR: Failed to create {} for {}!!\n".format(
                            self.project_slug,
                            target_account.data["display_name"])
                        self.log.error(msg)